import { Component, OnInit } from '@angular/core';
import { SharedService } from '../../../shared/services/shared.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  tempList: number[] = [1, 2, 3, 4, 5, 6];
  constructor(private _sharedService: SharedService, private _router: Router) { }

  ngOnInit() {
    this._sharedService.setTitle('Home');
  }

  goToCreateShow() {
    this._router.navigate(['shows/create']);
  }
}
