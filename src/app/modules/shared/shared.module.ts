import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedService } from './services/shared.service';
import { AuthService } from './services/auth.service';
import { BreadcrumbComponent } from './components/breadcrumb/breadcrumb.component';
import { BreadcrumbsComponent } from './components/breadcrumbs/breadcrumbs.component';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    RouterModule
  ],
  declarations: [BreadcrumbComponent, BreadcrumbsComponent],
  providers: [
    SharedService, AuthService
  ],
  exports: [BreadcrumbComponent, BreadcrumbsComponent]
})
export class SharedModule { }
