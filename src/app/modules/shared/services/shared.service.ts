import { Injectable } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Injectable({
  providedIn: 'root'
})
export class SharedService {
  readonly APP_TITLE_ENDING: string = ' - Show Planner';
  constructor(private _titleService: Title) { }

  public setTitle(newTitle: string) {
    this._titleService.setTitle(newTitle + this.APP_TITLE_ENDING);
  }
}
