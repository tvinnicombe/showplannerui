import { Injectable, OnInit } from '@angular/core';
import { Subject, Observable, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private _user: BehaviorSubject<User> = new BehaviorSubject(new User())
  public readonly user: Observable<User> = this._user.asObservable();

  setUser(name: string) {
    this._user.next(new User(name));
  }
}

export class User {
  constructor(name?: string) { this.Name = name != null ? name : 'Unknown'; }
  Name: string;
}