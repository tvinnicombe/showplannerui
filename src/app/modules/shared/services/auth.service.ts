import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private readonly REQUIRE_LOGIN = true;
  private _userIsAuthenticated: BehaviorSubject<boolean> = new BehaviorSubject(!this.REQUIRE_LOGIN);
  public readonly userIsAuthenticated: Observable<boolean> = this._userIsAuthenticated.asObservable();
  public userIsAuthenticatedLastValue: boolean = !this.REQUIRE_LOGIN;

  constructor() {
    // For temporary purposes until we integrate with backend.
    //if (!environment.production)
    //  this.authUser();
  }

  public authUser() {
    this._userIsAuthenticated.next(true);
    this.userIsAuthenticatedLastValue = true;
  }
}
