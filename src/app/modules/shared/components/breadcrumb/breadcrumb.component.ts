import { Component, OnInit, Input } from '@angular/core';
import { Routes } from '@angular/router';

@Component({
  selector: 'breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.scss']
})
export class BreadcrumbComponent implements OnInit {
  @Input() route: string = '';
  @Input() currentRoute: boolean = false;

  constructor() { }

  ngOnInit() {
  }

}
