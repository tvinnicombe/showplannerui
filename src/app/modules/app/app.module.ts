import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { RouterModule, Routes, CanActivate } from '@angular/router';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { DashboardModule } from '../dashboard/dashboard.module';
import { ShowsModule } from '../shows/shows.module';
import { EventsModule } from '../events/events.module';

import { DashboardComponent } from '../dashboard/pages/dashboard/dashboard.component';
import { ShowsComponent } from '../shows/pages/shows/shows.component';
import { EventsComponent } from '../events/pages/events/events.component';
import { LoginComponent } from '../login/pages/login/login.component';
import { LoginModule } from '../login/login.module';
import { SharedModule } from '../shared/shared.module';
import { AuthGuardService } from '../shared/services/auth-guard.service';
import { CreateShowComponent } from '../shows/pages/create-show/create-show.component';

const appRoutes: Routes = [
  { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuardService] },
  { path: 'login', component: LoginComponent },
  { path: 'shows', component: ShowsComponent, canActivate: [AuthGuardService] },
  { path: 'shows/create', component: CreateShowComponent, canActivate: [AuthGuardService] },
  { path: 'events', component: EventsComponent, canActivate: [AuthGuardService] },
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  { path: '**', component: DashboardComponent, canActivate: [AuthGuardService] }
];

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent
  ],
  imports: [
    FormsModule,
    BrowserModule,
    BrowserAnimationsModule,
    DashboardModule,
    ShowsModule,
    EventsModule,
    LoginModule,
    SharedModule,
    RouterModule.forRoot(
      appRoutes,
      { /*enableTracing: true*/ } // <-- debugging purposes only
    )
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

