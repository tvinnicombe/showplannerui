import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { AuthService } from '../../../shared/services/auth.service';

@Component({
  selector: 'navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  showNavbar: boolean = true;
  subscriptions: Subscription[] = [];
  constructor(private _authService: AuthService) { }

  ngOnInit() {
    this.subscriptions.push(
      this._authService.userIsAuthenticated.subscribe(authorised => {
        this.showNavbar = authorised
      })
    );
  }

}
