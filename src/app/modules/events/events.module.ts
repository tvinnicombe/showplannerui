import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EventsComponent } from './pages/events/events.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [EventsComponent]
})
export class EventsModule { }
