import { Component, OnInit } from '@angular/core';
import { SharedService } from '../../../shared/services/shared.service';
import { UserService } from '../../../shared/services/user.service';
import { Router } from '@angular/router';
import { AuthService } from '../../../shared/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  email: string;
  password: string;
  checkingPassword: boolean = false;

  constructor(private _sharedService: SharedService, private _userService: UserService, private _router: Router, private _authService: AuthService) { }

  ngOnInit() {
    this._sharedService.setTitle('Login');
  }

  login() {
    this.checkingPassword = true;
    /* Temporary fake sign in and fake authorisation of user. */
    window.setTimeout(() => {
      this._userService.setUser('Tom');
      this._authService.authUser();
      this._router.navigate(['dashboard']);
      this.checkingPassword = false;
    }, 2500);
  }
}
