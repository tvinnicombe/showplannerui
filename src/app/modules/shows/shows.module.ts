import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShowsComponent } from './pages/shows/shows.component';
import { CreateShowComponent } from './pages/create-show/create-show.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule
  ],
  declarations: [ShowsComponent, CreateShowComponent]
})
export class ShowsModule { }
